const db = require('better-sqlite3')('database.db');

db.prepare(/* sql */`
    create table if not exists profiles (
        fingerprint varchar(64) primary key,
        display_name varchar(32)
    );
`).run();

db.prepare(/* sql */`
    create table if not exists messages (
        id integer primary key,
        posted_by varchar(64),
        display_name varchar(32) not null,
        posted_on datetime not null default current_timestamp,
        private boolean not null,
        content text not null,
        foreign key (posted_by)
            references profiles (fingerprint)
    );
`).run();

process.on('exit', () => db.close());
process.on('SIGHUP', () => process.exit(128 + 1));
process.on('SIGINT', () => process.exit(128 + 2));
process.on('SIGTERM', () => process.exit(128 + 15));

module.exports = { db };
