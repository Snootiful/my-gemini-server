const { readFileSync, watchFile } = require('node:fs');
const gemini = require('gemini-server').default;
const Mustache = require('mustache');
const { db } = require('./database');

let config;
try {
    config = require('./config.json');
} catch {
    config = {};
}
let admins = require('./admins.json').map(fingerprint => fingerprint.replaceAll(':', '').toLowerCase());

const certFilePath = config.cert  ?? './tls/cert.pem';
const keyFilePath = config.key ?? './tls/key.pem';

watchFile('./admins.json', () => {
    admins = require('./admins.json').map(fingerprint => fingerprint.replaceAll(':', '').toLowerCase());
    console.log('Admins file reloaded');
});

watchFile(certFilePath, () => {
    app._cert = readFileSync(certFilePath);
});
watchFile(keyFilePath, () => {
    app._key = readFileSync(keyFilePath);
});

const getProfileStatement = db.prepare(/*sql*/`select * from profiles where fingerprint = ?`);
const insertProfileStatement = db.prepare(/*sql*/`insert into profiles (fingerprint) values (?)`);
const setDisplayNameStatement = db.prepare(/*sql*/`update profiles set display_name = ? where fingerprint = ?`);
const deleteProfileStatement = db.prepare(/*sql*/`delete from profiles where fingerprint = ?`);
const getMessagesStatement = db.prepare(/*sql*/`
    select m.id, m.display_name, p.display_name as current_name, datetime(m.posted_on, \'localtime\') as posted_on, m.content
    from messages m left join profiles p on p.fingerprint = m.posted_by
    where m.private = ? order by m.posted_on desc
`);
const insertMessageStatement = db.prepare(/*sql*/`insert into messages (posted_by, display_name, private, content) values (?, ?, ?, ?)`);
const deleteMessageStatement = db.prepare(/*sql*/`delete from messages where id = ?`);

/**
 * @param {import('gemini-server').Request} req
 * @returns {string|undefined}
 * */
function getProfile(req) {
    return req.cert?.fingerprint256 ? getProfileStatement.get(req.cert?.fingerprint256) : undefined;
}

/**
 * @param {import('gemini-server').Request} req
 */
function isAdmin(req) {
    return admins.includes(req.cert?.fingerprint256?.replaceAll(':', '')?.toLowerCase());
}

const app = gemini({
    cert: readFileSync(certFilePath),
    key: readFileSync(keyFilePath),
});

app.use((req, res, next) => {
    res.mustache = (filename, view, partials, tagsOrOptions) => {
        try {
            res.data(Mustache.render(readFileSync(filename).toString(), view, partials, tagsOrOptions), 'text/gemini');
        } catch (err) {
            console.error(err);
            res.status(42); // Failure during dynamic content generation
        }
    };
    next();
});

app.on('/robots.txt', (_, res) => res.data(readFileSync('./robots.txt').toString(), 'text/plain'));

app.on('', (req, res) => {
    res.file('./pages/index.gmi');
});

app.on('/terms', (_, res) => res.file('./pages/terms.gmi'));

app.on('/profile', (req, res) => {
    let profile = getProfile(req);

    let view = {
        hasIdentity: req.cert?.fingerprint256 != undefined,
        hasSubjectCN: req.cert?.subject?.CN != undefined,
        hasSubjectUID: req.cert?.subject?.UID != undefined,
        subjectCN: req.cert?.subject?.CN?.trim().slice(0, 32),
        subjectUID: req.cert?.subject?.UID?.trim().slice(0, 32),
        hasProfile: profile != undefined,
        profile,
    };
    res.mustache('./pages/profile/index.gmi', view);
});
app.on('/profile/make', gemini.requireCert, (req, res) => {
    let profile = getProfile(req);
    if (profile) return res.redirect('/profile');

    insertProfileStatement.run(req.cert?.fingerprint256);
    res.redirect('/profile');
});
app.on('/profile/forget', gemini.requireCert, (req, res) => {
    if (req.cert?.fingerprint256 != undefined) deleteProfileStatement.run(req.cert?.fingerprint256);
    res.redirect('/profile');
});
app.on('/profile/displayname', gemini.requireCert, (req, res) => {
    let profile = getProfile(req);
    if (profile == undefined) return res.redirect('/profile');

    if (!req.query && !req.url.href.endsWith('?')) {
        res.input('Enter a display name (max. 32 characters, blank to remove):');
    } else {
        let displayName = decodeURIComponent(req.query ?? '').replaceAll('\n', '').trim().slice(0, 32);
        if (displayName.length === 0) displayName = null;

        setDisplayNameStatement.run(displayName, req.cert?.fingerprint256);
        res.redirect('/profile');
    }
});

app.on('/gembook', (req, res) => {
    let profile = getProfile(req);
    let messages = getMessagesStatement.all(0);

    let view = {
        hasIdentity: req.cert?.fingerprint256 != undefined,
        hasProfile: profile != undefined,
        profile,
        admin: isAdmin(req),
        anyMessages: messages.length > 0,
        messages: messages.map(message => ({ ...message, content: message.content.replaceAll('\n', '\n> ') })),
    };
    res.mustache('./pages/guestbook/index.gmi', view);
});

function handleMessageRequest(req, res, private) {
    if (!req.query) {
        res.input(`Enter your ${private ? 'private ' : ''}message here:`);
    } else {
        let profile = getProfile(req);
        let message = decodeURIComponent(req.query);

        insertMessageStatement.run(req.cert?.fingerprint256, profile?.display_name ?? 'Anonymous', private ? 1 : 0, message);

        res.redirect('/gembook');
    }
}
app.on('/gembook/post', (req, res) => handleMessageRequest(req, res, false));
app.on('/gembook/privatepost', (req, res) => handleMessageRequest(req, res, true));

app.on('/gembook/admin', gemini.requireCert, (req, res) => {
    if (!isAdmin(req)) return res.status(61);

    let messages = getMessagesStatement.all(1);

    let view = {
        anyMessages: messages.length > 0,
        messages,
    };
    res.mustache('./pages/guestbook/admin.gmi', view);
});
app.on('/gembook/admin/remove/:id', gemini.requireCert, (req, res) => {
    if (!isAdmin(req)) return res.status(61);
    deleteMessageStatement.run(req.params.id);
    res.redirect(req.query ? '/gembook/admin' : '/gembook');
});


const port = config.port ?? 1965;

app.listen(port, () => {
    console.log(`Gemini server listening on port ${port}`);
});
